def echo(something)
  something
end

def shout(something)
  something.upcase
end

def repeat(something, times=2)
  Array.new(times, something).join(" ")
end

def start_of_word(word, num)
  word[0...num]
end

def first_word(sentence)
  sentence.split.first
end

def titleize(sentence)
  little_words = ["the", "over", "and"]
  new_words = sentence.split.each_with_index.map do |word, i|
    if little_words.include?(word) && i != 0
      word
    else
      word.capitalize
    end
  end
  new_words.join(" ")
end
