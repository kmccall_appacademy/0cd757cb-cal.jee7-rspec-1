def add(num1, num2)
  num1 + num2
end

def subtract(num1, num2)
  num1 - num2
end

def sum(arr)
  arr.reduce(0,:+)
end

def multiply(*args)
  args.reduce(:*)
end

def power(num, power)
  num ** power
end

def factorial(num)
  if num > 0
    (1..num).reduce(:*)
  else
    0
  end
end
