def latinize(word)
  vowels = "aeiou"
  new_word = word
  until vowels.include?(new_word[0].downcase)
    new_word = new_word[1..-1] + new_word[0]
    if new_word[-1] == "q" && new_word[0] == "u"
      new_word = new_word[1..-1] + new_word[0]
      break
    end
  end

  if word.capitalize == word
    new_word.capitalize + "ay"
  else
    new_word + "ay"
  end
end

def translate(sentence)
  words = sentence.split
  words.map! { |word| latinize(word) }
  words.join(" ")
end
